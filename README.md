# Coding Challenge
I put two attempts at solving the challenge in two directories:
- `./pad_whole_nums_go`
- `./pad_whole_nums_js`

## JavaScript Attempt
### Setup
To test the JavaScript solution you will need the following:
- Node.js: tested locally with version `v10.15.0`.
- npm: tested locally with version `6.4.1`.

To run the unit tests, run the following:
```shell
cd ./pad_whole_nums_js
# Install `jest` testing package.
npm install
npm test
```

### Explanation
If presented this problem _without_ a concern for performance, this is probably the type of
solution (i.e., leveraging regular expressions) I would reach for. That said, as it uses regular
expression string replacement, I'm not sure what the time complexity actually is. I would assume
JavaScript might do something along the lines of the following:
- Find a list of unique regular expression matches.
- For each item in the above, perform a "replace all" with the respective return string of the replacer function.

In terms of space complexity, I would assume this would require managing the size _n_ of the
initial input string and then the size of the output as well. The size of the output seems
dependent on the number _m_ of whole numbers within the input string and the pad length _l_. It
seems like the size would be the product of _m_ and _l_, so size complexity would be `O(n^2)`.

## Go Attempt
### Setup
The only requirement should be Go. I tested locally with version `go version go1.15 linux/amd64`.
To run the unit tests, run the following:
```shell
cd ./pad_whole_nums_go
go test
```

### Explanation
I made a separate attempt in Go because of the following:
- I wanted an alternate solution that did _not_ use regular expression.
- I thought it might be kinda fun and you guys use the language hehe.

Because this attempt does not use regular expressions, I might have a chance at walking through the
time complexity:
- I loop through the length _n_ of the input string.
- In each loop, I only do a capped number of operations (so far seems like `O(n)`).
- I loop through the length _n_ of input again (to actually fill the output).
- If I omit padding, I again believe I have a fixed number of operations (so this seems like `O(n)` as well).
- With padding however, I have to consider the following:
	- I could have _m_ < _n_ whole numbers within the input string.
	- For each whole number, I could have the difference _d_ of padding needed to be added.
- Because of the padding, I think I'd have `O(m*d + n)`, or simple `O(n^2)`.

With the current attempt, I believe space complexity mirrors the time complexity (above) and is
dependent on the product of _m_ and _d_.

### Concrete Example
When describing the above, I'm considering a case like the following:
```txt
I: "1.1.1.1", 10
```

In that case, we have _m=4_ and _d=9_. So, we'd have to assemble a buffer of 36 + _n_ where _n=7_.
To me, at least with how the attempts were approached, the space quickly starts to be more significant
for the size of the padding and the number of whole numbers. Similarly, because, at least in Go, I
iterate _d_ times to assemble the left pad, it seems like the number of operations really becomes
influenced by _m_ and _d_.


