package padwholenums

func PadWholeNums(input string, length int) string {
	aggPadLen := 0
	numLen := 0
	for i := 0; i < len(input); i++ {
		ch := input[i]
		if ch >= '0' && ch <= '9' {
			numLen++
		} else if numLen > 0 {
			if numLen < length {
				aggPadLen += length - numLen
			}
			numLen = 0
		}
	}
	if numLen > 0 && numLen < length {
		aggPadLen += length - numLen
	}
	if aggPadLen == 0 {
		return input
	}
	output := make([]byte, len(input)+aggPadLen)
	w := 0
	numLen = 0
	i := 0
	for i < len(input) {
		ch := input[i]
		if ch >= '0' && ch <= '9' {
			numLen++
		} else {
			if numLen > 0 {
				if numLen < length {
					tmp := make([]byte, length)
					diff := length - numLen
					j := 0
					for j < diff {
						tmp[j] = '0'
						j++
					}
					copy(tmp[j:], input[i-numLen:i])
					w += copy(output[w:], tmp[:])
				} else {
					w += copy(output[w:], input[i-numLen:i])
				}
				numLen = 0
			}
			w += copy(output[w:], input[i:i+1])
		}
		i++
	}
	if numLen > 0 {
		if numLen < length {
			tmp := make([]byte, length)
			diff := length - numLen
			j := 0
			for j < diff {
				tmp[j] = '0'
				j++
			}
			copy(tmp[j:], input[i-numLen:i])
			copy(output[w:], tmp[:])
		} else {
			copy(output[w:], input[i-numLen:i])
		}
	}
	return string(output)
}
