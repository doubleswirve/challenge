package padwholenums

import "testing"

func TestPadWholeNums(t *testing.T) {
	want := "James Bond 007"
	if got := PadWholeNums("James Bond 7", 3); got != want {
		t.Fatalf("got %s, want %s", got, want)
	}

	want = "PI=03.14"
	if got := PadWholeNums("PI=3.14", 2); got != want {
		t.Fatalf("got %s, want %s", got, want)
	}

	want = "It's 03:13pm"
	if got := PadWholeNums("It's 3:13pm", 2); got != want {
		t.Fatalf("got %s, want %s", got, want)
	}

	want = "It's 12:13pm"
	if got := PadWholeNums("It's 12:13pm", 2); got != want {
		t.Fatalf("got %s, want %s", got, want)
	}

	want = "000099UR001337"
	if got := PadWholeNums("99UR1337", 6); got != want {
		t.Fatalf("got %s, want %s", got, want)
	}

	// Extras
	want = "003.000"
	if got := PadWholeNums("3.0", 3); got != want {
		t.Fatalf("got %s, want %s", got, want)
	}

	want = "James Pond 0007"
	if got := PadWholeNums("James Pond 007", 4); got != want {
		t.Fatalf("got %s, want %s", got, want)
	}
}
