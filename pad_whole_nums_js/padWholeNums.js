const WHOLE_NUM_RGX = /\d+/g;

function padWholeNums(input, length) {
	return input.replace(WHOLE_NUM_RGX, (wholeNum) => {
		return padLeft(wholeNum, length);
	});
}

function padLeft(input, length) {
	let pad = "";
	let padLenNeeded = length - input.length;
	let i = 0;
	while (i < padLenNeeded) {
		pad += "0";
		i++;
	}
	return pad + input;
}

module.exports = padWholeNums;
