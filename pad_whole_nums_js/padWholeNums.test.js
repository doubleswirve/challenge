const padWholeNums = require("./padWholeNums.js");

test("1", () => {
	expect(padWholeNums("James Bond 7", 3)).toBe("James Bond 007");
});

test("2", () => {
	expect(padWholeNums("PI=3.14", 2)).toBe("PI=03.14");
});

test("3", () => {
	expect(padWholeNums("It's 3:13pm", 2)).toBe("It's 03:13pm");
});

test("4", () => {
	expect(padWholeNums("It's 12:13pm", 2)).toBe("It's 12:13pm");
});

test("5", () => {
	expect(padWholeNums("99UR1337", 6)).toBe("000099UR001337");
});

// Extra tests
test("6", () => {
	expect(padWholeNums("3.0", 3)).toBe("003.000");
});

test("7", () => {
	expect(padWholeNums("James Pond 007", 4)).toBe("James Pond 0007");
});
